﻿using System.Web;
using System.Web.Optimization;

namespace BookingApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/IE8").Include(
                      "~/Scripts/plugins/respond.min.js",
                      "~/Scripts/plugins/excanvas.min.js",
                      "~/Scripts/plugins/ie8.fix.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                      "~/Scripts/plugins/jquery.min.js",
                      "~/Scripts/plugins/bootstrap.min.js",
                      "~/Scripts/plugins/js.cookie.min.js",
                      "~/Scripts/plugins/jquery.slimscroll.min.js",
                      "~/Scripts/plugins/jquery.blockui.min.js",
                      "~/Scripts/plugins/bootstrap-switch.min.js",
                      "~/Scripts/plugins/moment.min.js",
                      "~/Scripts/plugins/bootstrap-datepaginator.min.js",
                      "~/Scripts/ui-daterangepicker.min.js",
                      "~/Scripts/plugins/daterangepicker.min.js",
                      "~/Scripts/plugins/sweetalert.min.js",
                      "~/Scripts/ui-sweetalert.min.js",
                      "~/Scripts/plugins/ui-blockui.min.js",
                      "~/Scripts/plugins/icheck.min.js",
                      "~/Scripts/plugins/bootstrap-maxlength.min.js",
                      "~/Scripts/plugins/bootstrap-datepicker.min.js",
                      "~/Scripts/plugins/bootstrap-timepicker.min.js",
                      "~/Scripts/plugins/bootstrap-datetimepicker.min.js",
                      "~/Scripts/plugins/horizontal-timeline.min.js",
                      "~/Scripts/plugins/jquery.repeater.min.js",
                      "~/Scripts/plugins/jquery.inputmask.bundle.min.js",                
                      "~/Scripts/plugins/select2.full.min.js",
                      "~/Scripts/plugins/toastr.min.js",
                      "~/Scripts/ui-toastr.min.js",
                      "~/Scripts/plugins/morris.min.js",
                      "~/Scripts/plugins/wNumb.min.js",
                      "~/Scripts/plugins/nouislider.min.js",
                      "~/Scripts/plugins/spin.min.js",
                      "~/Scripts/plugins/ladda.min.js",                     
                      "~/Scripts/plugins/raphael-min.js",
                      "~/Scripts/plugins/hammer.min.js",
                      "~/Scripts/plugins/jquery.easing.js",
                      "~/Scripts/plugins/jquery.mousewheel.js",
                      "~/Scripts/plugins/jquery.easypiechart.min.js",
                      "~/Scripts/plugins/mapplic.min.js",
                      "~/Scripts/datatable.js",
                      "~/Scripts/datatables.min.js",
                      "~/Scripts/datatables.bootstrap.js",
                      "~/Scripts/plugins/jquery.waypoints.min.js",
                      "~/Scripts/plugins/jquery.counterup.min.js",
                      "~/Scripts/plugins/jquery.sparkline.min.js",
                      "~/Scripts/app.min.js",
                      "~/Scripts/form-repeater.min.js",
                      "~/Scripts/components-bootstrap-switch.min.js",
                      "~/Scripts/components-select2.min.js",
                      "~/Scripts/components-date-time-pickers.min.js",
                      "~/Scripts/custom.js",
                      "~/Scripts/layout.min.js",
                      "~/Scripts/quick-sidebar.min.js",
                      "~/Scripts/quick-nav.min.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/plugins/font-awesome.min.css",
                      "~/Content/plugins/simple-line-icons.min.css",
                      "~/Content/plugins/bootstrap.min.css",
                      "~/Content/plugins/bootstrap-switch.min.css",
                      "~/Content/plugins/bootstrap-datepaginator.min.css",
                      "~/Content/plugins/daterangepicker.min.css",
                      "~/Content/plugins/bootstrap-timepicker.min.css",
                      "~/Content/plugins/sweetalert.css",
                      "~/Content/achievement.css",
                      "~/Content/datatables.min.css",
                      "~/Content/datatables.bootstrap.css",
                      "~/Content/plugins/nouislider.min.css",
                      "~/Content/plugins/all.css",
                      "~/Content/plugins/ladda-themeless.min.css",
                      "~/Content/plugins/select2.min.css",
                      "~/Content/plugins/select2-bootstrap.min.css",
                      "~/Content/plugins/invoice-2.min.css",
                      "~/Content/plugins/morris.css",
                      "~/Content/plugins/mapplic.css",
                      "~/Content/plugins/toastr.min.css",
                      "~/Content/css/components-rounded.min.css",
                      "~/Content/css/plugins.min.css",
                      "~/Content/css/layout.min.css",
                      "~/Content/css/custom.min.css"));


            bundles.Add(new StyleBundle("~/Content/ErrorCss").Include(
                      "~/Content/plugins/font-awesome.min.css",
                      "~/Content/plugins/simple-line-icons.min.css",
                      "~/Content/plugins/bootstrap.min.css",
                      "~/Content/plugins/bootstrap-switch.css",
                      "~/Content/css/components-rounded.min.css",
                      "~/Content/css/plugins.min.css", 
                      "~/Content/css/error.min.css"));


            bundles.Add(new StyleBundle("~/Content/LoginCss").Include(
                      "~/Content/plugins/font-awesome.min.css",
                      "~/Content/plugins/simple-line-icons.min.css",
                      "~/Content/plugins/bootstrap.min.css",
                      "~/Content/plugins/bootstrap-switch.css",
                      "~/Content/plugins/select2.min.css",
                      "~/Content/plugins/select2-bootstrap.min.css",
                      "~/Content/css/components-rounded.min.css",
                      "~/Content/plugins/toastr.min.css",
                      "~/Content/plugins/sweetalert.css",
                      "~/Content/css/plugins.min.css",
                      "~/Content/css/login-5.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/LoginScripts").Include(
                      "~/Scripts/plugins/jquery.min.js",
                      "~/Scripts/plugins/bootstrap.min.js",
                      "~/Scripts/plugins/js.cookie.min.js",
                      "~/Scripts/plugins/jquery.slimscroll.min.js",
                      "~/Scripts/plugins/jquery.blockui.min.js",
                      "~/Scripts/plugins/bootstrap-switch.min.js",
                      "~/Scripts/plugins/jquery.validate.min.js",
                      "~/Scripts/plugins/additional-methods.min.js",
                      "~/Scripts/plugins/select2.full.min.js",
                      "~/Scripts/plugins/jquery.backstretch.min.js",
                      "~/Scripts/app.min.js",
                      "~/Scripts/login-5.min.js"));

        }
    }
}
