﻿using BookingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.App_Start
{
    public class SuperUserAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                //Check if this user is a super user and allowed access to this part of the system
                if (bool.Parse(ApplicationUser.getPropertyValue("SuperUser")) == false)
                {
                    filterContext.HttpContext.Response.Redirect("~/Dashboard/MemberDash", true);
                }
            }
            catch (Exception ex)
            {
                filterContext.HttpContext.Response.Redirect("~/Authentication/Login", true);
            }


        }


    }
}