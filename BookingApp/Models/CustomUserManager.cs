﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models
{
    public class CustomUserManager : UserManager<ApplicationUser>
    {

        public CustomUserManager() : base(new CustomUserStore<ApplicationUser>())
        {

        }

        public static CustomUserManager Create()
        {
            var manager = new CustomUserManager();
            return manager;
        }

    }
}