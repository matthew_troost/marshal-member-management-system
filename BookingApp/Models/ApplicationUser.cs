﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace BookingApp.Models
{
    public class ApplicationUser : IdentityUser
    {

        //This is a class where we store all relevant info belonging to the user when they log in
        //We can just reference anything inside it whenever we need
        public virtual int UserID { get; set; }
        public virtual string Guid { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FullName { get; set; }
        public override string Email { get; set; }
        public virtual bool SuperUser { get; set; }
        public virtual string Password { get; set; }
        public virtual int StudioId { get; set; }
        public virtual bool Marshal { get; set; }

        public static List<Claim> populateClaims(ApplicationUser user)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim("UserID", user.UserID.ToString()));
            claims.Add(new Claim("Guid", user.Guid));
            claims.Add(new Claim("FirstName", $"{user.FirstName}"));
            claims.Add(new Claim("LastName", $"{user.LastName}"));
            claims.Add(new Claim("FullName", $"{user.FirstName} {user.LastName}"));
            claims.Add(new Claim("Email", $"{user.Email}"));
            claims.Add(new Claim("SuperUser", user.SuperUser.ToString()));
            claims.Add(new Claim("Password", user.Password.ToString()));
            claims.Add(new Claim("StudioId", user.StudioId.ToString()));
            claims.Add(new Claim("Marshal", user.Marshal.ToString()));
            return claims;
        }

        public static string getPropertyValue(string propertyName)
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            foreach (Claim c in principal.Claims)
            {
                if (c.Type == propertyName) return c.Value;
            }

            return "";
        }



    }
}