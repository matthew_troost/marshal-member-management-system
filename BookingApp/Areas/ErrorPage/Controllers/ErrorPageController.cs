﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.ErrorPage.Controllers
{
    public class ErrorPageController : Controller
    {

        public ActionResult Error404()
        {
            return View();
        }

        public ActionResult Error500()
        {
            return View();
        }

        public ActionResult Default()
        {
            return View();
        }
    }
}