﻿using System.Web.Mvc;

namespace BookingApp.Areas.ErrorPage
{
    public class ErrorPageAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ErrorPage";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ErrorPage_default",
                "ErrorPage/{action}/{id}",
                new { controller = "ErrorPage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}