﻿using BookingApp.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Studios.Controllers
{
    public class StudiosController : Controller
    {
        [Marshal]
        public ActionResult List()
        {
            return View();
        }
    }
}