﻿using System.Web.Mvc;

namespace BookingApp.Areas.Studios
{
    public class StudiosAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Studios";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Studios_default",
                "Studios/{action}/{id}",
                new { controller = "Studios", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}