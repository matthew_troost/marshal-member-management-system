﻿using System.Web.Mvc;

namespace BookingApp.Areas.Achievements
{
    public class AchievementsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Achievements";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Achievements_default",
                "Achievements/{action}/{id}",
                new { controller = "Achievements", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}