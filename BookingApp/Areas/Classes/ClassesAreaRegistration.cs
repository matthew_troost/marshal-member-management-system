﻿using System.Web.Mvc;

namespace BookingApp.Areas.Classes
{
    public class ClassesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Classes";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Classes_default",
                "Classes/{action}/{id}",
                new { controller = "Class", action = "Forecast", id = UrlParameter.Optional }
            );
        }
    }
}