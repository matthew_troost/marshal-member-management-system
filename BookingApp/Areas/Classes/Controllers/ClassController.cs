﻿using BookingApp.App_Start;
using BookingApp.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Classes.Controllers
{
    public class ClassController : Controller
    {
        [AllowAnonymous]
        public ActionResult CheckIn()
        {
            return View();
        }

        [SuperUser]
        public ActionResult Forecast()
        {
            return View();
        }

        [SuperUser]
        public void UpdateDefaultClasses()
        {
            Common.Class.updateDefaultClasses();
        }

        [SuperUser]
        public ActionResult DefaultClasses()
        {
            return View();
        }

        [SuperUser]
        public ActionResult ClassTypes()
        {
            return View(new ClassTypeModel());
        }

        [RegularUser]
        public ActionResult Book()
        {
            return View();
        }

        [RegularUser]
        public ActionResult RefreshBookings(string date, int studioId)
        {
            BookingModel model = BookingModel.getClassesForDay(date, studioId);

            return View(model);
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClassTypes(ClassTypeModel model)
        {
            if (model.ClassTypeId != 0) //updateing
                Common.Class.updateClassType(model.ClassTypeId, model.Name, model.MaxBookings);

            else //adding
                Common.Class.addClassType(model.Name, int.Parse(model.MaxBookings), int.Parse(ApplicationUser.getPropertyValue("StudioId")));

            return View(new ClassTypeModel { Success = true });
        }


 
        public JsonResult AddClass(int day, int month, int year, string time, int classTypeId, int studioId)
        {
            return Json(Common.Class.addClass(day,month,year,time,classTypeId,studioId), JsonRequestBehavior.AllowGet);
        }

 
        public JsonResult UpdateClass(int id, string time, int classTypeId)
        {
            Common.Class.updateClass(id, time, classTypeId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

 
        public JsonResult DeleteClass(int id)
        {
            return Json(Common.Class.deleteClass(id), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetClass(int id)
        {
            return Json(Common.Class.getClass(id), JsonRequestBehavior.AllowGet);
        }


        public JsonResult AddDefaultClass(int dayOfWeek, string time, int classTypeId, int studioId)
        {
            return Json(Common.Class.addDefaultClass(dayOfWeek, time, classTypeId, studioId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteDefaultClass(int id)
        {
            return Json(Common.Class.deleteDefaultClass(id), JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteClassType(int id)
        {
            return Json(Common.Class.deleteClassType(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClassType(int id)
        {
            return Json(Common.Class.getClassType(id), JsonRequestBehavior.AllowGet);
        }



        public JsonResult AddBooking(int classId, int userId, int studioId, string date)
        {
            return Json(Common.Booking.addBooking(classId,userId,studioId,date), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CancelBooking(int id, int classId)
        {
            return Json(Common.Booking.deleteBooking(id, classId), JsonRequestBehavior.AllowGet);
        }


        public JsonResult AddtoWaitingList(int classId, int userId, int studioId, string date)
        {
            return Json(Common.Booking.addToWaitingList(classId, userId, studioId, date), JsonRequestBehavior.AllowGet);
        }
    }
}