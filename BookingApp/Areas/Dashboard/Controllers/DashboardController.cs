﻿using BookingApp.App_Start;
using BookingApp.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Dashboard.Controllers
{
    public class DashboardController : Controller
    {
        [SuperUser]
        public ActionResult Dashboard()
        {
            return View();
        }

        [RegularUser]
        public ActionResult MemberDash()
        {
            return View();
        }

        [SuperUser]
        public ActionResult RefreshClasses(int classID)
        {
            ClassListModel model = ClassListModel.getBookingsForClass(classID, int.Parse(ApplicationUser.getPropertyValue("StudioId")));

            return View(model);
        }
    }
}