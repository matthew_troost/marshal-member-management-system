﻿using System.Web.Mvc;

namespace BookingApp.Areas.Autherization
{
    public class AutherizationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Autherization";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Autherization_default",
                "Autherization/{action}/{id}",
                new { controller = "Autherization", action = "Login", id = UrlParameter.Optional }
            );

        }
    }
}