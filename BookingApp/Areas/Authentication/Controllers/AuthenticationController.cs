﻿using Microsoft.Owin.Security;
using Models;
using System;
using System.Web;
using System.Web.Mvc;
using Common.Database;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using BookingApp.Models;

namespace BookingApp.Areas.Authentication.Controllers
{
    public class AuthenticationController : Controller
    {
        private IAuthenticationManager AuthenticationManager { get { return HttpContext.GetOwinContext().Authentication; } }

        //[AllowAnonymous]
        //public ActionResult Login()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        //if (bool.Parse(ApplicationUser.getPropertyValue("SuperUser")) == true)
        //        //    return RedirectToAction("Analytics", "Dashboard", new { area = "Dashboard" });
        //        //else
        //        //    return RedirectToAction("MyProfile", "Profiles", new { area = "Profiles" });

        //        return RedirectToAction("Dashboard", "Dashboard", new { area = "Dashboard" });
        //    }

        //    LoginModel model = new LoginModel();

        //    return View(model);
        //}


        [AllowAnonymous]
        public ActionResult Login(string recentlySignedUp)
        {
            if (string.IsNullOrEmpty(recentlySignedUp))
            {
                if (User.Identity.IsAuthenticated)
                    return RedirectToAction("Dashboard", "Dashboard", new { area = "Dashboard" });

                LoginModel model = new LoginModel();

                return View(model);
            }
            else {
                LoginModel model = new LoginModel { RecentlySignedUp = "True" };

                return View(model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please enter your credentials");
                return View(model);
            }

            user user = Common.User.login(model.Username, model.Password);

            if (user == null)
            {
                //return that it failed
                model.Failed = true;
                return View(model);
            }

            //Log them in
            //create the custom application user
            var userClaim = new ApplicationUser()
            {
                Id = user.id.ToString(),
                UserName = user.username,
                Guid = user.guid,
                UserID = user.id,
                FirstName = user.name,
                LastName = user.surname,
                FullName = $"{user.name} {user.surname}",
                Email = user.email,
                SuperUser = user.superUser == true,
                Password = user.password,
                StudioId = (int)user.studio_id,
                Marshal = user.marshal == true,
                SecurityStamp = Guid.NewGuid().ToString(),
            };

            //CustomUserManager userManager = HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            CustomUserManager userManager = new CustomUserManager();

            //create the user identity which will be stored in the httpcontext.current.user property going forward
            ClaimsIdentity identity = userManager.CreateIdentity(userClaim, DefaultAuthenticationTypes.ApplicationCookie);

            //add each custom property as a claim so we can access it once the user is logged in (without pulling it from the db)
            identity.AddClaims(ApplicationUser.populateClaims(userClaim));

            AuthenticationProperties authProperties = new AuthenticationProperties();
            authProperties.ExpiresUtc = DateTime.UtcNow.AddDays(!model.RememberMe ? 3 : 45);

            //sign the user in
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(authProperties, identity);

            //FormsAuthentication.SetAuthCookie(ApplicationUser.getPropertyValue("id").ToString(), false);

            userManager.Dispose();

            //if (user.superUser == true)
            //    return RedirectToAction("Analytics", "Dashboard", new { area = "Dashboard" });
            //else
                return RedirectToAction("Dashboard", "Dashboard", new { area = "Dashboard" });

        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login");
        }


        [AllowAnonymous]
        public ActionResult Register(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Login");

            StudioModel studioModel = StudioModel.getStudio(id);

            if (studioModel == null)
                return RedirectToAction("Login");

            UserModel model = new UserModel { Guid = studioModel.Guid, StudioName = studioModel.Name, StudioId = studioModel.StudioId, SuperUser = false };

            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserModel model)
        {
            //if (!Common.Studio.incrementMemberCount(model.StudioId))
            //{
            //    //return failed message - too full
            //}

            int newUserId = Common.User.addUser(model.Name, model.Surname, model.UserName, model.Password, model.Email, false, model.StudioId, model.ContactNumber);

            return RedirectToAction("Login", new { recentlySignedUp = "True" });
        }


    }
}