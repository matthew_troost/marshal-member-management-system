﻿using BookingApp.App_Start;
using BookingApp.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Members.Controllers
{
    public class MembersController : Controller
    {
        [SuperUser]
        public ActionResult List()
        {
            return View();
        }

        [SuperUser]
        public ActionResult Single()
        {
            return View(new UserModel());
        }

        [SuperUser]
        public ActionResult Memberships()
        {
            return View(new MembershipModel());
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Memberships(MembershipModel model)
        {
            if (model.MembershipId == 0)
                Common.Membership.addMembership(model.Price, model.MaxBookings, int.Parse(ApplicationUser.getPropertyValue("StudioId")), model.Name);
            else
                Common.Membership.updateMembership(model.MembershipId, model.Price, model.MaxBookings, model.Name);

            return View(new MembershipModel { Success = true });
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Single(UserModel model)
        {
            int studioId = int.Parse(ApplicationUser.getPropertyValue("StudioId"));

            //if (!Common.Studio.incrementSuperUserCount(studioId))
            //{
            //    //return failed message - too full
            //}

            Common.User.addUser(model.Name, model.Surname, model.UserName, model.Password, model.Email, true, studioId, model.ContactNumber);

            return View(new UserModel { RecentlyAdded = true });
        }

        public JsonResult DeleteUser(int id)
        {
            return Json(Common.User.deleteUser(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteMembershipType(int id)
        {
            return Json(Common.Membership.deleteMembership(id), JsonRequestBehavior.AllowGet);
        }

    }
}