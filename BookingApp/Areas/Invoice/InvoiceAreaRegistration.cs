﻿using System.Web.Mvc;

namespace BookingApp.Areas.Invoice
{
    public class InvoiceAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Invoice";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Invoice_default",
                "Invoice/{action}/{id}",
                new { controller = "Invoice", action = "New", id = UrlParameter.Optional }
            );
        }
    }
}