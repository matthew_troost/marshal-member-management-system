﻿using BookingApp.App_Start;
using BookingApp.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Invoice.Controllers
{
    public class InvoiceController : Controller
    {
        [SuperUser]
        public ActionResult New()
        {
            SettingsModel model = SettingsModel.get(int.Parse(ApplicationUser.getPropertyValue("StudioId")));

            return View(model);
        }

        public JsonResult GetMemberDetails(int userId)
        {
            return Json(Common.User.getSummaryOfUser(int.Parse(ApplicationUser.getPropertyValue("StudioId")), userId), JsonRequestBehavior.AllowGet);
        }


    }
}