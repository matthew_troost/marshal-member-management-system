﻿using System.Web.Mvc;

namespace BookingApp.Areas.Emails
{
    public class EmailsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Emails";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Emails_default",
                "Emails/{action}/{id}",
                new { controller = "Emails", action = "Inbox", id = UrlParameter.Optional }
            );
        }
    }
}