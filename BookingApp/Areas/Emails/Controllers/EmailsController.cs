﻿using BookingApp.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Emails.Controllers
{
    public class EmailsController : Controller
    {
        [Marshal]
        public ActionResult Inbox()
        {
            return View();
        }
    }
}