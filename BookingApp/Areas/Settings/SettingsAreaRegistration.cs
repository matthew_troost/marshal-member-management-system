﻿using System.Web.Mvc;

namespace BookingApp.Areas.Settings
{
    public class SettingsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Settings";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Settings_default",
                "Settings/{action}/{id}",
                new { controller = "Settings", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}