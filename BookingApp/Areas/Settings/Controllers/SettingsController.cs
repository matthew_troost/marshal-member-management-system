﻿using BookingApp.App_Start;
using BookingApp.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Areas.Settings.Controllers
{
    public class SettingsController : Controller
    {

        [SuperUser]
        public ActionResult List()
        {

            SettingsModel model = SettingsModel.get(int.Parse(ApplicationUser.getPropertyValue("StudioId")));

            return View(model);
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateStudioDetails(SettingsModel model)
        {
            Common.Studio.updateStudioDetails(int.Parse(ApplicationUser.getPropertyValue("StudioId")), model.Name, model.Email, int.Parse(model.TelNumber), model.Address, model.Postal_code);

            return RedirectToAction("List"); 
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateBankDetails(SettingsModel model)
        {
            if (model.BankDetailsId == 0)
                Common.Settings.addBankDetails(int.Parse(ApplicationUser.getPropertyValue("StudioId")), model.PayeeName, model.Bank, model.BranchCode, model.AccNumber, model.VatNumber);
            else
                Common.Settings.updateBankDetails(int.Parse(ApplicationUser.getPropertyValue("StudioId")), model.PayeeName, model.Bank, model.BranchCode, model.AccNumber, model.VatNumber);


            return RedirectToAction("List");
        }

    }
}