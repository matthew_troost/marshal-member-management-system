﻿using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserModel
    {

        public int UserId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime DateJoined { get; set; }

        public string Email { get; set; }

        public string Guid { get; set; }

        public int StudioId { get; set; }

        public string StudioName { get; set; }

        public bool SuperUser { get; set; }

        public string ContactNumber { get; set; }

        public bool RecentlyAdded { get; set; }


        public static IEnumerable<UserModel> getAll(int studioId)
        {
            foreach (user item in Common.User.getAll(studioId))
            {
                yield return bindModel(item);
            }
        }

        public static UserModel get(int id)
        {
            return bindModel(Common.User.get(id));
        }

        public static UserModel bindModel(user item)
        {
            return new UserModel
            {
                UserId = item.id,
                Name = item.name,
                Surname = item.surname,
                UserName = item.username,
                Password = item.password,
                DateJoined = (DateTime)item.date_joined,
                Email = item.email,
                Guid = item.guid,
                StudioId = (int)item.studio_id,
                SuperUser = (bool)item.superUser,
                ContactNumber = item.contact_number
            };
        }


    }
}
