﻿using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class DefaultClassModel
    {

        public int ClassId { get; set; }

        public TimeSpan Time { get; set; }

        public string Name { get; set; }

        public int StudioId { get; set; }

        public int DayOfWeek { get; set; }

        public int MaximumBookings { get; set; }


        public static IEnumerable<DefaultClassModel> getForDay(int dayOfWeek, int studioId)
        {
            foreach (DefaultClass_custom item in Common.Class.getDefaultClassesForDay(dayOfWeek, studioId))
            {
                yield return bindModel(item);
            }
        }

        public static DefaultClassModel bindModel(DefaultClass_custom item)
        {
            return new DefaultClassModel
            {
                ClassId = item.id,
                Name = item.name,
                StudioId = item.studioId,
                DayOfWeek = item.day_of_week,
                MaximumBookings = item.max_bookings,
                Time = item.time
            };
        }


    }
}
