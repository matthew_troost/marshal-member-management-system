﻿using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class MembershipModel
    {

        public int MembershipId { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public int MaxBookings { get; set; }

        public int StudioId { get; set; }

        public bool Success { get; set; }


        public static IEnumerable<MembershipModel> getMemberships(int studioId)
        {
            foreach (membership item in Common.Membership.getMemberships(studioId))
            {
                yield return bindModel(item);
            }
        }

        public static MembershipModel bindModel(membership item)
        {
            return new MembershipModel
            {
                MembershipId = item.id,
                Name = item.name,
                Price = (int)item.price,
                MaxBookings = (int)item.max_bookings,
                StudioId = (int)item.studioId
            };
        }



    }
}
