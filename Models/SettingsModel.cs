﻿using Common.Database;
using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class SettingsModel
    {
        public int StudioId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string TelNumber { get; set; }

        public string Address { get; set; }

        public int Postal_code { get; set; }


        public int BankDetailsId { get; set; }

        public string AccNumber { get; set; }

        public string Bank { get; set; }

        public string BranchCode { get; set; }

        public string PayeeName { get; set; }

        public string VatNumber { get; set; }


        public int MaxMembers { get; set; }

        public int MaxSuperUsers { get; set; }

        public string PaymentPlan { get; set; }

        public int PaymentPlanPrice { get; set; }



        public static SettingsModel get(int studioId)
        {
            SettingsModel model = new SettingsModel();

            studio studio = Common.Studio.getStudio(studioId);
            model.StudioId = studio.id;
            model.Name = studio.name;
            model.Email = studio.email;
            model.TelNumber = "0" + studio.telNumber.ToString();
            model.Address = studio.address;
            model.Postal_code = (int)studio.postal_code;

            payment_plan plan = Common.Settings.getPaymentPlan((int)studio.payment_planID);
            if (plan != null)
            {
                model.PaymentPlan = plan.name;
                model.PaymentPlanPrice = (int)plan.price;
                model.MaxMembers = (int)plan.max_members;
                model.MaxSuperUsers = (int)plan.max_superUsers;
            }

            bank_details details = Common.Settings.getBankDetails(studioId);
            if (details != null)
            {
                model.BankDetailsId = details.id;
                model.AccNumber = details.accNumber;
                model.Bank = details.bank;
                model.BranchCode = details.branchCode;
                model.PayeeName = details.payeeName;
                model.VatNumber = details.vat_number;
            }

            return model;
        }


    }
}
