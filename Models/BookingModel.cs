﻿using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class BookingModel
    {

        public IEnumerable<Class_custom> Classes { get; set; }

        public DateTime Date { get; set; }


        public static BookingModel getClassesForDay(string date, int studioId)
        {
            return new BookingModel { Classes = Common.Class.getClassesForDay(date, studioId), Date = DateTime.Parse(date) };
        }


    }
}
