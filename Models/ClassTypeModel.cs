﻿using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ClassTypeModel
    {
        public int ClassTypeId { get; set; }

        public bool Success { get; set; }

        public string Name { get; set; }

        public string MaxBookings { get; set; }


    }
}
