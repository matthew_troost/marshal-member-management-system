﻿using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class StudioModel
    {

        public int StudioId { get; set; }

        public string Name { get; set; }

        public string Guid { get; set; }

        public int TotalMembers { get; set; }

        public int TotalSuperUsers { get; set; }

        public long TelNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public int PostalCode { get; set; }


        public static StudioModel getStudio(string id)
        {
            return bindModel(Common.Studio.getStudio(id));
        }

        public static StudioModel getStudio(int id)
        {
            return bindModel(Common.Studio.getStudio(id));
        }

        public static StudioModel bindModel(studio item)
        {
            return new StudioModel
            {
                StudioId = item.id,
                Name = item.name,
                Guid = item.guid,
                TotalMembers = (int)item.total_members,
                TotalSuperUsers = (int)item.total_superUsers,
                TelNumber = (long)item.telNumber,
                Email = item.email,
                Address = item.address,
                PostalCode = (int)item.postal_code
            };
        }


    }
}
