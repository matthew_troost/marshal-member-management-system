﻿using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ClassModel
    {

        public int ClassId { get; set; }

        public TimeSpan Time { get; set; }

        public string Name { get; set; }

        public int StudioId { get; set; }

        public DateTime Date { get; set; }

        public int Booked { get; set; }

        public int MaximumBookings { get; set; }


        public static IEnumerable<ClassModel> getForDay(string date, int studioId)
        {
            foreach (Class_custom item in Common.Class.getClassesForDay(date, studioId))
            {
                yield return bindModel(item);
            }
        }

        public static ClassModel bindModel(Class_custom item)
        {
            return new ClassModel
            {
                ClassId = item.id,
                Name = item.name,
                StudioId = item.studioId,
                Date = item.date,
                Booked = item.number_booked,
                MaximumBookings = item.max_bookings,
                Time = item.time
            };
        }



    }
}
