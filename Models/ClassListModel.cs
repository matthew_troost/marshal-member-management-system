﻿using Common.Database.Custom_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ClassListModel
    {

        public IEnumerable<Booking_custom> Bookings { get; set; }

        public IEnumerable<WaitingList_Custom> WaitingList { get; set; }

        public string ClasssName { get; set; }


        public static ClassListModel getBookingsForClass(int classId, int studioId)
        {

            return new ClassListModel { Bookings = Common.Booking.getBookingsForClass(classId, studioId, DateTime.Today.ToString("yyyy-MM-dd")),
                ClasssName = Common.Class.getClass(classId).name,
                WaitingList = Common.Booking.getWaitingListForClass(classId) };
        }

    }
}
