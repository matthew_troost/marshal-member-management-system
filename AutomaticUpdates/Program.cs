﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Database.Custom_Models;
using BookingApp.Models;
using Common.Database;

namespace AutomaticUpdates
{
    public class Program
    {
        static void Main(string[] args)
        {

            foreach (studio studio in Studio.getAll())
            {


                DateTime date = new DateTime(2017, 8, 7); //Monday
                DateTime forecastDate = DateTime.Today;
                int counter = 0;


                //first Week
                while (date.DayOfWeek.ToString() != "Sunday")
                {
                    //fetch default classes for each day
                    IEnumerable<DefaultClass_custom> classes = Class.getDefaultClassesForDay(counter, studio.id);

                    if (forecastDate.DayOfWeek == date.DayOfWeek)
                    {
                        if (Class.getClassesForDay(forecastDate.ToString("yyyy-MM-dd"), studio.id).Count() == 0) //if there are no default classes currently
                        {
                            foreach (DefaultClass_custom clas in classes)
                            {
                                //add each default class to the class list for the forecast
                                Class.addClass(forecastDate.Day, forecastDate.Month, forecastDate.Year, clas.time.ToString(), clas.class_type_Id, clas.studioId);
                            }
                        }

                        forecastDate = forecastDate.AddDays(1);
                    }

                    counter++;
                    date = date.AddDays(1);
                }


                //Back to Monday again
                date = date.AddDays(1);
                counter = 0;
                forecastDate = forecastDate.AddDays(1);


                //second Week
                while (date.DayOfWeek.ToString() != "Sunday")
                {
                    //fetch default classes for each day
                    IEnumerable<DefaultClass_custom> classes = Class.getDefaultClassesForDay(counter, studio.id);

                    if (forecastDate.DayOfWeek == date.DayOfWeek)
                    {
                        if (Class.getClassesForDay(forecastDate.ToString("yyyy-MM-dd"), studio.id).Count() == 0) //if there are no default classes currently
                        {
                            foreach (DefaultClass_custom clas in classes)
                            {
                                //add each default class to the class list for the forecast
                                Class.addClass(forecastDate.Day, forecastDate.Month, forecastDate.Year, clas.time.ToString(), clas.class_type_Id, clas.studioId);
                            }
                        }

                        forecastDate = forecastDate.AddDays(1);
                    }

                    counter++;
                    date = date.AddDays(1);
                }







            }

        }
    }
}
