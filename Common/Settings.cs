﻿using Common.Database;
using Common.Database.Custom_Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Settings
    {

        public static Settings_custom getSettings(int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"SELECT s.`name`, s.email, s.telNumber, s.address, s.vat_number, s.postal_code, b.accNumber, b.bank, b.branchCode, b.payeeName, p.max_members, p.max_superUsers, p.`name` as payment_plan_name, p.price as payment_plan_price FROM studios s
                            inner join bank_details b on b.studioId = s.id
                            inner join payment_plan p on p.id = s.payment_planID
                            where b.deleted = 0
                            and s.deleted = 0
                            and p.deleted = 0
                            and s.id = @studioId;";

            using (var context = new gymentor_test_databaseEntities())
            {
                Settings_custom settings = context.Database.SqlQuery<Settings_custom>(sql, pars.ToArray()).FirstOrDefault();
                return settings;
            }
        }

        public static bank_details getBankDetails(int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"SELECT * FROM bank_details
                            where deleted = 0
                            and studioId = @studioId;";

            using (var context = new gymentor_test_databaseEntities())
            {
                bank_details details = context.Database.SqlQuery<bank_details>(sql, pars.ToArray()).FirstOrDefault();
                return details;
            }
        }

        public static payment_plan getPaymentPlan(int paymentPlanId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@paymentPlanId", paymentPlanId));

            string sql = @"SELECT * FROM payment_plan
                            where deleted = 0
                            and id = @paymentPlanId;";

            using (var context = new gymentor_test_databaseEntities())
            {
                payment_plan plan = context.Database.SqlQuery<payment_plan>(sql, pars.ToArray()).FirstOrDefault();
                return plan;
            }
        }


        public static void updateBankDetails(int studioId, string payeeName, string bank, string branchCode, string accNumber, string vat_number)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@studioId", studioId));
                pars.Add(new MySqlParameter("@payeeName", payeeName));
                pars.Add(new MySqlParameter("@bank", bank));
                pars.Add(new MySqlParameter("@branchCode", branchCode));
                pars.Add(new MySqlParameter("@accNumber", accNumber));
                pars.Add(new MySqlParameter("@vat_number", vat_number));

                string sql = @"update bank_details set
                                payeeName = @payeeName,
                                bank = @bank,
                                accNumber = @accNumber,
                                branchCode = @branchCode,
                                vat_number = @vat_number
                                where studioId = @studioId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static int addBankDetails(int studioId, string payeeName, string bank, string branchCode, string accNumber, string vat_number)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@studioId", studioId));
                pars.Add(new MySqlParameter("@payeeName", payeeName));
                pars.Add(new MySqlParameter("@bank", bank));
                pars.Add(new MySqlParameter("@branchCode", branchCode));
                pars.Add(new MySqlParameter("@accNumber", accNumber));
                pars.Add(new MySqlParameter("@vat_number", vat_number));

                string sql = "";

                if (vat_number != "" || vat_number != null) { 

                    sql = @"insert into bank_details
                                    (payeeName,bank,accNumber,branchCode,vat_number,studioId)
                                    values
                                    (@payeeName,@bank,@accNumber,@branchCode,@vat_number,@studioId);
                                    SELECT LAST_INSERT_ID() as id;";
                }
                else
                {
                    sql = @"insert into bank_details
                                (payeeName,bank,accNumber,branchCode,studioId)
                                values
                                (@payeeName,@bank,@accNumber,@branchCode,@studioId);
                                SELECT LAST_INSERT_ID() as id;";
                }


                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

    }
}
