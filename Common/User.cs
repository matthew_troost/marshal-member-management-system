﻿using Common.Database;
using Common.Database.Custom_Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class User
    {

        public static user login(string Username, string Password)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@Username", Username));
            pars.Add(new MySqlParameter("@Password", Password));

            string sql = "select * from user where (username = @Username or email = @Username) and password = @Password";

            using (var context = new gymentor_test_databaseEntities())
            {
                user user = context.Database.SqlQuery<user>(sql, pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static IEnumerable<user> getAll(int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"select * from user 
                            where studio_id = @studioId 
                            and deleted = 0
                            order by superUser desc, `name` asc;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<user> users = context.Database.SqlQuery<user>(sql, pars.ToArray()).ToList();
                return users;
            }
        }


        public static IEnumerable<UserSummary_custom> getSummary(int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"SELECT u.*, m.max_bookings, m.name as membershipName, m.price 
                            FROM user u
                            inner join membership m on m.id = u.membershipId
                            where u.studio_id = @studioId
                            and u.deleted = 0
                            and m.deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<UserSummary_custom> users = context.Database.SqlQuery<UserSummary_custom>(sql, pars.ToArray()).ToList();
                return users;
            }
        }

        public static UserSummary_custom getSummaryOfUser(int studioId, int userId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@userId", userId));
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"SELECT u.*, m.max_bookings, m.name as membershipName, m.price 
                            FROM user u
                            inner join membership m on m.id = u.membershipId
                            where u.studio_id = @studioId
                            and u.id = @userId
                            and u.deleted = 0
                            and m.deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                UserSummary_custom user = context.Database.SqlQuery<UserSummary_custom>(sql, pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static user get(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select * from user 
                            where id = @id 
                            and deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                user user = context.Database.SqlQuery<user>(sql, pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static int addUser(string name, string surname, string username, string password, string email, bool superUser, int studioId, string contactNumber)
        {

            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@surname", surname));
                pars.Add(new MySqlParameter("@username", username));
                pars.Add(new MySqlParameter("@password", password));
                pars.Add(new MySqlParameter("@email", email));
                pars.Add(new MySqlParameter("@superUser", superUser));
                pars.Add(new MySqlParameter("@studioId", studioId));
                pars.Add(new MySqlParameter("@contactNumber", contactNumber));

                string sql = @"insert into user
                                (guid,name,surname,username,password,email,superUser,studio_id,contact_number)
                                values
                                (@Guid,@name,@surname,@username,@password,@email,@superUser,@studioId,@contactNumber);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

        public static bool deleteUser(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update user set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }


    }
}
