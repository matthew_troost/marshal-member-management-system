﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Membership
    {
        public static int addMembership(int price, int max_bookings, int studioId, string name)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@price", price));
                pars.Add(new MySqlParameter("@max_bookings", max_bookings));
                pars.Add(new MySqlParameter("@studioId", studioId));
                pars.Add(new MySqlParameter("@name", name));

                string sql = @"insert into membership
                                (price,max_bookings,studioId,name)
                                values
                                (@price,@max_bookings,@studioId,@name);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();

                return id;
            }
        }


        public static void updateMembership(int id, int price, int max_bookings, string name)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@price", price));
                pars.Add(new MySqlParameter("@max_bookings", max_bookings));
                pars.Add(new MySqlParameter("@name", name));

                string sql = @"update membership set
                                price = @price,
                                max_bookings = @max_bookings,
                                name = @name
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        public static bool deleteMembership(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update membership set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                return true;
            }
        }


        public static IEnumerable<membership> getMemberships(int studioID)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioID", studioID));

            string sql = @"select * from membership
                            where studioId = @studioID
                            and deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<membership> memberships = context.Database.SqlQuery<membership>(sql, pars.ToArray()).ToList();
                return memberships;
            }
        }


    }
}
