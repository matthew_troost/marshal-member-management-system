﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Studio
    {
        public static IEnumerable<studio> getAll()
        {
            string sql = @"select *
                            from studios 
                            where deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<studio> studios = context.Database.SqlQuery<studio>(sql).ToArray();
                return studios;
            }
        }

        public static studio getStudio(string guid)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@guid", guid));

            string sql = @"select *
                            from studios 
                            where guid = @guid
                            and deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();
                return studio;
            }
        }

        public static studio getStudio(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select *
                            from studios 
                            where id = @id
                            and deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();
                return studio;
            }
        }

        public static void updateStudioDetails(int studioId, string name, string email, int telNumber, string address, int postalCode)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@studioId", studioId));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@email", email));
                pars.Add(new MySqlParameter("@telNumber", telNumber));
                pars.Add(new MySqlParameter("@address", address));
                pars.Add(new MySqlParameter("@postalCode", postalCode));

                string sql = @"update studios set
                                name = @name,
                                telNumber = @telNumber,
                                address = @address,
                                postal_code = @postalCode,
                                email = @email
                                where id = @studioId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        //public static bool incrementMemberCount(int studioId)
        //{
        //    using (var context = new gymentor_test_databaseEntities())
        //    {
        //        List<MySqlParameter> pars = new List<MySqlParameter>();
        //        pars.Add(new MySqlParameter("@studioId", studioId));

        //        string sql = @"select *
        //                        from studios 
        //                        where id = @studioId;";

        //        studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();

        //        if (studio.total_members == studio.max_members)
        //            return false;

        //        sql = @"update studios set
        //                    total_members = (total_members + 1)
        //                    where id = @studioId";

        //        context.Database.ExecuteSqlCommand(sql, pars.ToArray());
        //        return true;
        //    }
        //}


        //public static bool decrementMemberCount(int studioId)
        //{
        //    using (var context = new gymentor_test_databaseEntities())
        //    {
        //        List<MySqlParameter> pars = new List<MySqlParameter>();
        //        pars.Add(new MySqlParameter("@studioId", studioId));

        //        string sql = @"select *
        //                        from studios 
        //                        where id = @studioId;";

        //        studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();

        //        if (studio.total_members == 0)
        //            return false;

        //        sql = @"update studios set
        //                    total_members = (total_members - 1)
        //                    where id = @studioId";

        //        context.Database.ExecuteSqlCommand(sql, pars.ToArray());
        //        return true;
        //    }
        //}

        //public static bool incrementSuperUserCount(int studioId)
        //{
        //    using (var context = new gymentor_test_databaseEntities())
        //    {
        //        List<MySqlParameter> pars = new List<MySqlParameter>();
        //        pars.Add(new MySqlParameter("@studioId", studioId));

        //        string sql = @"select *
        //                        from studios 
        //                        where id = @studioId;";

        //        studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();

        //        if (studio.total_superUsers == studio.max_superUsers)
        //            return false;

        //        sql = @"update studios set
        //                    total_superUsers = (total_superUsers + 1)
        //                    where id = @studioId";

        //        context.Database.ExecuteSqlCommand(sql, pars.ToArray());
        //        return true;
        //    }
        //}


        //public static bool decrementSuperUserCount(int studioId)
        //{
        //    using (var context = new gymentor_test_databaseEntities())
        //    {
        //        List<MySqlParameter> pars = new List<MySqlParameter>();
        //        pars.Add(new MySqlParameter("@studioId", studioId));

        //        string sql = @"select *
        //                        from studios 
        //                        where id = @studioId;";

        //        studio studio = context.Database.SqlQuery<studio>(sql, pars.ToArray()).FirstOrDefault();

        //        if (studio.total_superUsers == 0)
        //            return false;

        //        sql = @"update studios set
        //                    total_superUsers = (total_superUsers - 1)
        //                    where id = @studioId";

        //        context.Database.ExecuteSqlCommand(sql, pars.ToArray());
        //        return true;
        //    }
        //}

    }
}
