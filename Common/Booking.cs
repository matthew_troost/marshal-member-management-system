﻿using Common.Database;
using Common.Database.Custom_Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Booking
    {
        public static int addBooking(int classID, int userID, int studioID, string date)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classID", classID));
                pars.Add(new MySqlParameter("@userID", userID));
                pars.Add(new MySqlParameter("@studioID", studioID));
                pars.Add(new MySqlParameter("@date", date));

                string sql = @"insert into booking
                                (classId,userId,studioId,date)
                                values
                                (@classID,@userID,@studioID,@date);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();

                //update booking count for class
                incrementBookingCount(classID);

                return id;
            }
        }

        public static IEnumerable<Booking_custom> getBookingsForClass(int classID, int studioID, string date)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@classID", classID));
            pars.Add(new MySqlParameter("@studioID", studioID));
            pars.Add(new MySqlParameter("@date", date));

            string sql = @"select b.*, u.name, u.surname
                            from booking b
                            inner join user u on u.id = b.userId
                            where b.studioId = @studioID
                            and b.`date` = @date
                            and b.classId = @classID
                            and b.deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<Booking_custom> bookings = context.Database.SqlQuery<Booking_custom>(sql, pars.ToArray()).ToList();
                return bookings;
            }
        }


        public static void incrementBookingCount(int classId)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classId", classId));

                string sql = @"update s_classes set
                                number_booked = number_booked + 1
                                where id = @classId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static void decrementBookingCount(int classId)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classId", classId));

                string sql = @"update s_classes set
                                number_booked = number_booked - 1
                                where id = @classId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool deleteBooking(int id, int classId)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update booking set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                decrementBookingCount(classId);

                return true;
            }
        }



        public static int addToWaitingList(int classID, int userID, int studioID, string date)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classID", classID));
                pars.Add(new MySqlParameter("@userID", userID));
                pars.Add(new MySqlParameter("@studioID", studioID));
                pars.Add(new MySqlParameter("@date", date));

                string sql = @"insert into waiting_list
                                (classId,userId,studioId,date)
                                values
                                (@classID,@userID,@studioID,@date);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

        public static IEnumerable<WaitingList_Custom> getWaitingListForClass(int classID)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@classID", classID));

            string sql = @"select b.*, u.name, u.surname
                            from waiting_list b
                            inner join user u on u.id = b.userId
                            where b.classId = @classID
                            and b.deleted = 0;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<WaitingList_Custom> list = context.Database.SqlQuery<WaitingList_Custom>(sql, pars.ToArray()).ToList();
                return list;
            }
        }


        public static bool removeFromWaitingList(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update waiting_list set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                return true;
            }
        }


    }
}
