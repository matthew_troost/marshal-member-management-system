//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class booking
    {
        public int id { get; set; }
        public Nullable<int> classId { get; set; }
        public Nullable<int> userId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<bool> deleted { get; set; }
        public Nullable<int> studioId { get; set; }
    }
}
