﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.Custom_Models
{
    public class Settings_custom
    {

        public string name { get; set; }
        public string email { get; set; }
        public long telNumber { get; set; }
        public string address { get; set; }
        public int postal_code { get; set; }
        public int accNumber { get; set; }
        public string bank { get; set; }
        public string branchCode { get; set; }
        public string payeeName { get; set; }
        public int max_members { get; set; }
        public int max_superUsers { get; set; }
        public string payment_plan_name { get; set; }
        public int payment_plan_price { get; set; }
        public int vat_number { get; set; }



    }
}
