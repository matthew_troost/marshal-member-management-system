﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.Custom_Models
{
    public class WaitingList_Custom
    {
        public int id { get; set; }

        public int classId { get; set; }

        public int userId { get; set; }

        public int studioId { get; set; }

        public DateTime date { get; set; }

        public string name { get; set; }

        public string surname { get; set; }

        public bool deleted { get; set; }

    }
}
