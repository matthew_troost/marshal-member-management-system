﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.Custom_Models
{
    public class Class_custom
    {
        public int id { get; set; }

        public int class_type_Id { get; set; }

        public TimeSpan time { get; set; }

        public int studioId { get; set; }

        public DateTime date { get; set; }

        public int number_booked { get; set; }

        public int max_bookings { get; set; }

        public string name { get; set; }
    }
}
