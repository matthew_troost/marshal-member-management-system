﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.Custom_Models
{
    public class DefaultClass_custom
    {

        public int id { get; set; }

        public int class_type_Id { get; set; }

        public TimeSpan time { get; set; }

        public int studioId { get; set; }

        public int day_of_week { get; set; }

        public int max_bookings { get; set; }

        public string name { get; set; }

        public bool deleted { get; set; }

    }
}
