﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.Custom_Models
{
    public class UserSummary_custom
    {

        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> date_joined { get; set; }
        public string email { get; set; }
        public string guid { get; set; }
        public Nullable<bool> superUser { get; set; }
        public Nullable<int> studio_id { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string contact_number { get; set; }
        public Nullable<int> membershipId { get; set; }
        public Nullable<int> bookingsMade { get; set; }
        public string membershipName { get; set; }
        public Nullable<int> max_bookings { get; set; }
        public Nullable<int> price { get; set; }

    }
}
