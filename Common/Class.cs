﻿using Common.Database;
using Common.Database.Custom_Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Class
    {

        public static int addClass(int day, int month, int year, string time, int classTypeId, int StudioId)
        {
            DateTime date = new DateTime(year,month,day);
            string[] timePieces = time.Split(':');
            TimeSpan dtime = new TimeSpan(int.Parse(timePieces[0]), int.Parse(timePieces[1]), 0);

            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classTypeId", classTypeId));
                pars.Add(new MySqlParameter("@time", dtime));
                pars.Add(new MySqlParameter("@studioId", StudioId));
                pars.Add(new MySqlParameter("@date", date));

                string sql = @"insert into s_classes
                                (class_type_Id,time,studioId,date)
                                values
                                (@classTypeId,@time,@studioId,@date);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

        public static void updateClass(int id, string time, int classTypeId)
        {
            string[] timePieces = time.Split(':');
            TimeSpan dtime = new TimeSpan(int.Parse(timePieces[0]), int.Parse(timePieces[1]), 0);

            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@time", time));
                pars.Add(new MySqlParameter("@classTypeId", classTypeId));

                string sql = @"update s_classes set
                                time = @time,
                                class_type_Id = @classTypeId
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        public static bool deleteClass(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update s_classes set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static IEnumerable<Class_custom> getClassesForDay(string date, int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@date", date));
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"select c.*, t.name, t.max_bookings
                            from s_classes c
                            inner join class_type t on c.class_type_Id = t.id
                            where c.studioId=@studioId
                            and c.date = @date
                            and c.deleted = 0
                            order by time;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<Class_custom> classes = context.Database.SqlQuery<Class_custom>(sql, pars.ToArray()).ToList();
                return classes;
            }
        }

        public static Class_custom getClass(int classId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@classId", classId));

            string sql = @"select c.*, t.name, t.max_bookings
                            from s_classes c
                            inner join class_type t on c.class_type_Id = t.id
                            and c.deleted = 0
                            and c.id = @classId;";

            using (var context = new gymentor_test_databaseEntities())
            {
                Class_custom classes = context.Database.SqlQuery<Class_custom>(sql, pars.ToArray()).FirstOrDefault();
                return classes;
            }
        }



        public static int addDefaultClass(int dayOfWeek, string time, int classTypeId, int StudioId)
        {
            string[] timePieces = time.Split(':');
            TimeSpan dtime = new TimeSpan(int.Parse(timePieces[0]), int.Parse(timePieces[1]), 0);

            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@classTypeId", classTypeId));
                pars.Add(new MySqlParameter("@time", dtime));
                pars.Add(new MySqlParameter("@studioId", StudioId));
                pars.Add(new MySqlParameter("@dayOfWeek", dayOfWeek));

                string sql = @"insert into default_class
                                (class_type_Id,time,studioId,day_of_week)
                                values
                                (@classTypeId,@time,@studioId,@dayOfWeek);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

        public static bool deleteDefaultClass(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update default_class set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static IEnumerable<DefaultClass_custom> getDefaultClassesForDay(int dayOfWeek, int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@dayOfWeek", dayOfWeek));
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"select c.*, t.name, t.max_bookings
                            from default_class c
                            inner join class_type t on c.class_type_Id = t.id
                            where c.studioId=@studioId
                            and c.day_of_week = @dayOfWeek
                            and c.deleted = 0
                            order by time;";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<DefaultClass_custom> classes = context.Database.SqlQuery<DefaultClass_custom>(sql, pars.ToArray()).ToList();
                return classes;
            }
        }


        public static int addClassType(string name, int maxBookings, int studioId)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@maxBookings", maxBookings));
                pars.Add(new MySqlParameter("@studioId", studioId));

                string sql = @"insert into class_type
                                (name,max_bookings,studioId)
                                values
                                (@name,@maxBookings,@studioId);
                                SELECT LAST_INSERT_ID() as id;";

                int classTypeId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return classTypeId;
            }
        }

        public static void updateClassType(int id, string name, string maxBookings)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@maxBookings", maxBookings));

                string sql = @"update class_type set
                                name = @name,
                                max_bookings = @maxBookings
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool deleteClassType(int id)
        {
            using (var context = new gymentor_test_databaseEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update class_type set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static IEnumerable<class_type> getClassTypes(int studioId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@studioId", studioId));

            string sql = @"select *
                            from class_type 
                            where studioId=@studioId
                            and deleted = 0";

            using (var context = new gymentor_test_databaseEntities())
            {
                IEnumerable<class_type> types = context.Database.SqlQuery<class_type>(sql, pars.ToArray()).ToList();
                return types;
            }
        }

        public static class_type getClassType(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select *
                            from class_type 
                            where id=@id
                            and deleted = 0";

            using (var context = new gymentor_test_databaseEntities())
            {
                class_type type = context.Database.SqlQuery<class_type>(sql, pars.ToArray()).FirstOrDefault();
                return type;
            }
        }




        public static void updateDefaultClasses()
        {

            foreach (studio studio in Studio.getAll())
            {

                DateTime date = new DateTime(2017, 8, 7); //Monday
                DateTime forecastDate = DateTime.Today;
                int counter = 1;


                //first Week
                while (date.DayOfWeek.ToString() != "Sunday")
                {
                    //fetch default classes for each day
                    IEnumerable<DefaultClass_custom> classes = getDefaultClassesForDay(counter, studio.id);

                    if (forecastDate.DayOfWeek == date.DayOfWeek)
                    {
                        if (getClassesForDay(forecastDate.ToString("yyyy-MM-dd"), studio.id).Count() == 0) //if there are no default classes currently
                        {
                            foreach (DefaultClass_custom clas in classes)
                            {
                                //add each default class to the class list for the forecast
                                addClass(forecastDate.Day, forecastDate.Month, forecastDate.Year, clas.time.ToString(), clas.class_type_Id, clas.studioId);
                            }
                        }

                        forecastDate = forecastDate.AddDays(1);
                    }

                    counter++;
                    date = date.AddDays(1);
                }


                //Back to Monday again
                date = date.AddDays(1);
                counter = 1;
                forecastDate = forecastDate.AddDays(1);


                //second Week
                while (date.DayOfWeek.ToString() != "Sunday")
                {
                    //fetch default classes for each day
                    IEnumerable<DefaultClass_custom> classes = getDefaultClassesForDay(counter, studio.id);

                    if (forecastDate.DayOfWeek == date.DayOfWeek)
                    {
                        if (getClassesForDay(forecastDate.ToString("yyyy-MM-dd"), studio.id).Count() == 0) //if there are no default classes currently
                        {
                            foreach (DefaultClass_custom clas in classes)
                            {
                                //add each default class to the class list for the forecast
                                addClass(forecastDate.Day, forecastDate.Month, forecastDate.Year, clas.time.ToString(), clas.class_type_Id, clas.studioId);
                            }
                        }

                        forecastDate = forecastDate.AddDays(1);
                    }

                    counter++;
                    date = date.AddDays(1);
                }







            }



        }
















    }
}
